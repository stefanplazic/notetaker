NoteTaker is an app for taking your notes. In main window you have the list of all notes that you have taken. They are sorted by the date of last modification. 

By long press you have options to delete the note. By clicking + button you go to activity for adding new note.