package com.stefan.acer.notetaker;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class NewNoteActivity extends ActionBarActivity {

    //elements
    private EditText newTitleEdit,newTextEdit;
    private Button addNewbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        //inicialize
        newTitleEdit= (EditText) findViewById(R.id.newTitleEdit);
        newTextEdit= (EditText) findViewById(R.id.newTextEdit);
        addNewbtn= (Button) findViewById(R.id.addNewbtn);
        //set clicklistener
        addNewbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNew();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //crete new note
    public void createNew()
    {
        String titleText = newTitleEdit.getText().toString();
        String noteText = newTextEdit.getText().toString();
        if (titleText.trim().length()>0 && noteText.trim().length()>0)
        {
            //empty the fields
            newTitleEdit.setText("");
            newTextEdit.setText("");
            //make new note
            Note newNote = new Note();
            newNote.setNoteText(noteText);
            newNote.setNoteTitle(titleText);
            newNote.setNoteDate(System.currentTimeMillis());
            NoteDataBase noteDataBase = new NoteDataBase(this);
            noteDataBase.noteInsert(newNote);
            //Toast.makeText(this,titleText+" note is created",Toast.LENGTH_SHORT).show();
            finish();
        }
        else
        {
            Toast.makeText(this,"You must enter text and title",Toast.LENGTH_SHORT).show();
        }
    }
}
