package com.stefan.acer.notetaker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer on 27.06.2015.
 */
public class NoteDataBase extends SQLiteOpenHelper {

    //fields
    private  static  final  String TABLE_NOTES="Notes";
    private  static  final  String COLUM_ID="noteId";
    private  static  final  String COLUM_TITLE="noteTitle";
    private  static  final  String COLUM_TEXT="noteText";
    private  static  final  String COLUM_DATE="noteDate";

    //database settings
    private  static final  int DATABASE_VERSION=1;
    private static  final  String DATABASE_NAME="notes.db";

    //database creation strings
    private  static  final String CREATE_STATMENT="create table "+TABLE_NOTES+" ("+COLUM_ID+" integer primary key autoincrement, "+COLUM_TITLE+" text not null, "+COLUM_TEXT+" text not null, "
            +COLUM_DATE+" integer not null);";

    //constructor
    public  NoteDataBase(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_STATMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NOTES);
        onCreate(db);
    }
    //insert note
    public  void  noteInsert(Note newNote)
    {
        SQLiteDatabase sqlDb = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //put values into contentValues
        contentValues.put(COLUM_TITLE,newNote.getNoteTitle());
        contentValues.put(COLUM_TEXT,newNote.getNoteText());
        contentValues.put(COLUM_DATE,newNote.getNoteDate());
        //insert into database
        sqlDb.insert(TABLE_NOTES, null, contentValues);
        sqlDb.close();
    }

    //delete note
    public void  noteDelete(Note noteToDelete)
    {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_NOTES, COLUM_ID + " = ?", new String[]{String.valueOf(noteToDelete.getNoteId())});
        sqLiteDatabase.close();
    }
    //get all notes
    public List<Note> notesGetAll()
    {
        List<Note> notes = new ArrayList<Note>();
        String query = "SELECT * FROM "+TABLE_NOTES+" ORDER BY "+COLUM_DATE+" DESC";
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);

        //while loop

        if(cursor.moveToFirst())
        {
            do
            {
                Note note = new Note();
                //populate the note object
                note.setNoteId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUM_ID))));
                note.setNoteTitle(cursor.getString(cursor.getColumnIndex(COLUM_TITLE)));
                note.setNoteText(cursor.getString(cursor.getColumnIndex(COLUM_TEXT)));
                note.setNoteDate(Long.parseLong(cursor.getString(cursor.getColumnIndex(COLUM_DATE))));

                //add note to notes
                notes.add(note);
            }
            while (cursor.moveToNext());
        }

        return notes;

    }

    //update note
    public int noteUpdate(Note noteToUpdate)
    {
        SQLiteDatabase sqLiteDatabase =this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //put values
        contentValues.put(COLUM_TITLE,noteToUpdate.getNoteTitle());
        contentValues.put(COLUM_TEXT,noteToUpdate.getNoteText());
        contentValues.put(COLUM_DATE,noteToUpdate.getNoteDate());
        //run the query
        int i = sqLiteDatabase.update(TABLE_NOTES,contentValues,COLUM_ID+" = ?", new String[]{String.valueOf(noteToUpdate.getNoteId())});
        sqLiteDatabase.close();

        return  i;
    }
}
