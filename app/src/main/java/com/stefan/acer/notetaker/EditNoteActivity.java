package com.stefan.acer.notetaker;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;


public class EditNoteActivity extends ActionBarActivity {
private EditText noteTextChange;
    private  Note editNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        noteTextChange=(EditText)findViewById(R.id.noteTextChange);
        putText();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            this.editNote.setNoteText(this.noteTextChange.getText().toString());
            this.editNote.setNoteDate(System.currentTimeMillis());
            //save into database
            NoteDataBase noteDataBase = new NoteDataBase(this);
            noteDataBase.noteUpdate(editNote);
            finish();

            return true;
        }
        if(id==R.id.action_delete)
        {
            NoteDataBase noteDataBase = new NoteDataBase(this);
            noteDataBase.noteDelete(this.editNote);
            finish();
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }

    //put text into field
    public void  putText()
    {
        Intent intent = getIntent();
         editNote = (Note) intent.getSerializableExtra("editNote");
        //set title of activity
        setTitle(editNote.getNoteTitle());

        noteTextChange.setText(editNote.getNoteText());
    }
}
