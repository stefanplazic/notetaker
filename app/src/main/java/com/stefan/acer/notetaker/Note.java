package com.stefan.acer.notetaker;

import android.text.format.DateFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Acer on 27.06.2015.
 */
public class Note implements Serializable{
    private int noteId;
    private String noteTitle;
    private  String noteText;
    private long noteDate;

    //consturctor
    public  Note()
    {

    }


    //setters and getters
    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }


    public long getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(long noteDate) {
        this.noteDate = noteDate;
    }
}
