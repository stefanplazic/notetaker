package com.stefan.acer.notetaker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Acer on 28.06.2015.
 */
public class NotesAdapter extends ArrayAdapter<Note> {
    public NotesAdapter(Context context, ArrayList<Note> notes) {
        super(context, 0, notes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Note note = getItem(position);
        if(convertView==null)
        {
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.note_view_item,parent,false);
        }
        TextView TitleLabel = (TextView)convertView.findViewById(R.id.TitleLabel);
        TextView DateLabel = (TextView)convertView.findViewById(R.id.DateLabel);
        TitleLabel.setText(note.getNoteTitle());
        String dateFormat = new SimpleDateFormat("dd.MM.yyyy").format(new Date(note.getNoteDate()));
        DateLabel.setText(dateFormat);

        return  convertView;

    }
}
