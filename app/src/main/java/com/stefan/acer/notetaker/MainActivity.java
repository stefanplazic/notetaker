package com.stefan.acer.notetaker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    //variables
    private NoteDataBase noteDataBase ;
    private ArrayList<Note> noteArrayList ;
    private  ListView viewContainer ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.noteDataBase =new NoteDataBase(this);
        this.viewContainer = (ListView)findViewById(R.id.viewContainer);
        viewContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               GotoEdit(position);
            }
        });
        //on longclick
        viewContainer.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                NoteDelete(position);

                return true;
            }
        });
        RefreshContainer();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            //start new activity
            Intent intent = new Intent(this,NewNoteActivity.class);
            startActivity(intent);
            return true;
        }
        if(id == R.id.action_share)
        {
            ShareApp();
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }

    //method for test
    public  void  RefreshContainer()
    {

        List<Note> notes= noteDataBase.notesGetAll();
        if(notes.size()!=0){




            //cust list to arraylist
             noteArrayList = (ArrayList<Note>)notes;
            NotesAdapter notesAdapter = new NotesAdapter(this,noteArrayList);
            //find view by id

            viewContainer.setAdapter(notesAdapter);
            //set on itemclick listener

        }


    }



    //go to edit activity if listitem is clicked
    public  void GotoEdit(int position)
    {
        Note sendNote= noteArrayList.get(position);
        //send the note
        Intent intent = new Intent(MainActivity.this,EditNoteActivity.class);
        intent.putExtra("editNote",sendNote);
        startActivity(intent);
    }

    //delete the note
    public  void NoteDelete(final int position)
    {
         new AlertDialog.Builder(this).setTitle("Delete").setMessage("Do you want to delete?").setPositiveButton("Delete", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 //delete the note
                 Note note = noteArrayList.get(position);
                 noteDataBase.noteDelete(note);
                 RefreshContainer();
                 dialog.dismiss();

             }
         }).setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 dialog.dismiss();
             }
         }).show();
    }
    public  void Testing()
    {NoteDataBase noteDataBase = new NoteDataBase(this);

        for (int i=0;i<10;i++)
        {
            Note n = new Note();
            n.setNoteTitle("Number "+i);
            n.setNoteText("Text " + i);
            n.setNoteDate(System.currentTimeMillis());
            noteDataBase.noteInsert(n);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        RefreshContainer();
    }

    //sharing method
    public  void ShareApp()
    {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        String shareString ="I love using this crazy app!";
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"NoteTaker ");
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareString);

        startActivity(Intent.createChooser(shareIntent, "Share NoteTaker"));
    }
}
